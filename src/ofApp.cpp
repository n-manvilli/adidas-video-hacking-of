#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {

	// Prepare app window
	ofSetFrameRate(FRAME_RATE);
	ofSetVerticalSync(true);
	
	// Prepare webcam
	cam.setup(CAM_WIDTH, CAM_HEIGHT);
	cam.setDesiredFrameRate(CAM_FRAME_RATE);

	// Prepare Server
	ofxLibwebsockets::ServerOptions options = ofxLibwebsockets::defaultServerOptions();
	options.port = TCP_PORT;
	options.bUseSSL = false;
	server.setup(options);
	server.addListener(this);
	
	// Set detection timer as expired
	detectionTimer = DETECTION_TIMEOUT;

}

//--------------------------------------------------------------
void ofApp::update() {

	sendMovements = false;
	if ( cam.isInitialized() ) {

		// Update webcam image
		cam.update();
		camPixels = cam.getPixels();

		// Get brightest point
		maxBrightness = 0;
		maxBrightnessX = 0;
		maxBrightnessY = 0;
		for (int x = 0; x < CAM_WIDTH; x++) {
			for (int y = 0; y < CAM_HEIGHT; y++) {

				ofColor pointColor = camPixels.getColor(x, y);
				float pointBrightness = pointColor.getBrightness();

				if (pointBrightness > maxBrightness) {
					maxBrightness = pointBrightness;
					maxBrightnessX = x;
					maxBrightnessY = y;
				}

			}
		}

		// Check if the conditions are right to send the position
		if (maxBrightness >= BRIGHTNESS_TRESHOLD) {
			detectionTimer = 0;
			pointerX = maxBrightnessX;
			pointerY = maxBrightnessY;
			sendMovements = true;
		}
		else if (detectionTimer < DETECTION_TIMEOUT) {
			detectionTimer++;
			sendMovements = true;
		}

		if (sendMovements) {
			/* Send movemend coordinates in JSON:
			*	{"cmd":"mv", "x": POSX,"y": POSY,"b": BRIGHTNESS}
			*/
			toSend = "{\"cmd\":\"mv\", \"x\":" + ofToString(pointerX) + ", \"y\":" + ofToString(pointerY) + ", \"b\":" + ofToString(maxBrightness) + "}";
			server.send(toSend);
			cout << toSend << endl;
		}
		else {
			// Send stop command
			toSend = "{\"cmd\":\"st\"}";
		}

	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	cam.draw(0,0);

	//if (sendMovements) {
		ofPushStyle();
		ofSetColor(255, 0, 0);
		ofNoFill();
		ofDrawCircle(maxBrightnessX, maxBrightnessY, 40);
		ofPopStyle();
	//}
}

//--------------------------------------------------------------
void ofApp::onConnect(ofxLibwebsockets::Event& args) {
	cout << "on connected" << endl;
}

//--------------------------------------------------------------
void ofApp::onOpen(ofxLibwebsockets::Event& args) {
	cout << "new connection open" << endl;
}

//--------------------------------------------------------------
void ofApp::onClose(ofxLibwebsockets::Event& args) {
	cout << "on close" << endl;
}

//--------------------------------------------------------------
void ofApp::onIdle(ofxLibwebsockets::Event& args) {
	cout << "on idle" << endl;
}

//--------------------------------------------------------------
void ofApp::onMessage(ofxLibwebsockets::Event& args) {
	cout << "got message " << args.message << endl;
}

//--------------------------------------------------------------
void ofApp::onBroadcast(ofxLibwebsockets::Event& args) {
	cout << "got broadcast " << args.message << endl;
}
