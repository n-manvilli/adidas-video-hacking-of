#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"

#define USE_WINDOW

//========================================================================
int main() {

	#ifdef USE_WINDOW
		cout << "Hello Window!" << endl;
		ofSetupOpenGL(640, 480, OF_WINDOW);
	#else
		cout << "Hello Console!" << endl;
		ofAppNoWindow window;
		ofSetupOpenGL(&window, 640, 480, OF_WINDOW);
	#endif

	ofRunApp(new ofApp());

}
