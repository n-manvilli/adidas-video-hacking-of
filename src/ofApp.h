#pragma once

#include "ofMain.h"
#include "ofxLibwebsockets.h"

class ofApp : public ofBaseApp {

public:

	// Base configuration
	const int DETECTION_TIMEOUT = 12; // expressed in number of frames
	const int BRIGHTNESS_TRESHOLD = 100;
	const int FRAME_RATE = 60;
	const int CAM_FRAME_RATE = 25;
	const int CAM_WIDTH = 640;
	const int CAM_HEIGHT = 480;
	const int TCP_PORT = 8989;

	// Base OF methods
	void setup();
	void update();
	void draw();

	// Class that handles the webcam input
	ofVideoGrabber cam;
	ofPixels camPixels;

	// Send coordinates while it is true
	bool sendMovements;
	
	/* Timer used to keep pointer in position in case of brief
	 * interruptions of the light.
	 */
	int detectionTimer;
	
	// Information about the light detected
	float maxBrightness;
	int maxBrightnessX;
	int maxBrightnessY;
	
	// Current pointer position
	int pointerX;
	int pointerY;

	// Websocket server manager
	ofxLibwebsockets::Server server;

	// Message to send through the socket
	string toSend;

	// Websocket methods
	void onConnect(ofxLibwebsockets::Event& args);
	void onOpen(ofxLibwebsockets::Event& args);
	void onClose(ofxLibwebsockets::Event& args);
	void onIdle(ofxLibwebsockets::Event& args);
	void onMessage(ofxLibwebsockets::Event& args);
	void onBroadcast(ofxLibwebsockets::Event& args);

};