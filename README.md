
**INTRODUCTION TO THE PROJECT**

This OpenFrameworks project is born from the desire to create an experience that can have a double application:
* Web
* Physical Experience (Interaction)

The idea is to create a web application that in addition to the standard operations, through the steps that we will explain later, is able to use a device "hacked" as if it were a mouse.

We want to create an installation that allows users to draw on a projected screen, without touching it, and using any hacked object like a spray can. 

So we want to simulate a "graffiti-like" experience on a digital wall. 

To do this we need the following tools:

* Projector
* Rear-projection black cloth
* Low intensity Laser o IR device
* An empty spray-can or another “hackered” object. 
* Raspberry or Mobile Pc
* Usb Cam (with a filter that allows only IR light to pass) 


***POINTER DETECTION***

At a basic level, what this application does is finding the brightest spot in the image acquired by the webcam.

Since the webcam has a filter that blocks all non-IR lights, the task is easier and more precise, and the light from the projection is not detected.

The basic loop used to detect the brightest point is as follows:
```
for (int x = 0; x < CAM_WIDTH; x++) {
	for (int y = 0; y < CAM_HEIGHT; y++) {

		ofColor pointColor = camPixels.getColor(x, y);
		float pointBrightness = pointColor.getBrightness();

		if (pointBrightness > maxBrightness) {
			maxBrightness = pointBrightness;
			maxBrightnessX = x;
			maxBrightnessY = y;
		}

	}
}
```

We also ignore noise by setting a brightness treshold, and we ignore short interruptions of the light detection by setting a timer (if the light is not found for less than a DETECTION_TIMEOUT, th application considers the pointer still in the last known position).

Then we send the position through a WebSocket (more on this later) in a JSON string format (the cmd value ‘mv’ stands for “pointer moved”):
```
{"cmd":"mv", "x": POSITION_X, "y": POSITION_Y, "b": BRIGHTNESS_VALUE}
```

If the pointer is not detected for a time that is more than the DETECTION_TIMEOUT, another message is sent to inform that the stroke is finished:
```
{"cmd":"st"}
```


***WEB SOCKET***

To allow the openframeworks program to communicate with the browser, we used WebSocket, a web technology that provides communication channels.
Specifically, we used a specific library of "ofxLibwebsockets". Thanks to this addons you can pass the coordinates of the brightest point to the browser that, therefore, can be used for the web application.

***How to add ofxLibwebsockets to a project***

Adding an addons to a project is very easy, after downloading the addons: 
    
* Insert the unzipped downloaded file in the addons folder of OpenFrameworks. 
* Open the project generator;
* Select the project in which the addons will be inserted.
* Select the addons ofxLibwebsockets in the “addons entry”.

***Web Socket code explanation***

In ofApp.h File:
```
// Websocket server manager
ofxLibwebsockets::Server server;
```

Also, add required websocket methods.
```
// Websocket methods
void onConnect(ofxLibwebsockets::Event& args);
void onOpen(ofxLibwebsockets::Event& args);
void onClose(ofxLibwebsockets::Event& args);
void onIdle(ofxLibwebsockets::Event& args);
void onMessage(ofxLibwebsockets::Event& args);
void onBroadcast(ofxLibwebsockets::Event& args);
```

In ofApp.cpp file, setup() method, create a variable “ofxLibwebsockets::Server” for the running and the communication with the server.
```
// Prepare Server
ofxLibwebsockets::ServerOptions options = ofxLibwebsockets::defaultServerOptions();
options.port = TCP_PORT;
options.bUseSSL = false;
server.setup(options);
server.addListener(this);
```

Where needed, create a string in which we are going to insert everything we want to communicate, then send it.
```
string myMessage = "message";
server.send(myMessage);
```

***Receive data from a browser***

Javascript example:
```
var connection = new WebSocket('ws://127.0.0.1:8989');

connection.onmessage = function (e) {
	// Message received
    console.log(e.data);
};
```


***INSTALLATION***

* Download the project files.
* If you use an IDE, create an appropriate project and add the files to it.
* Compile.


***FUTURE DEVELOPMENTS***

We plan to furhter refine the detection by applying the computer vision principles illustrated here:
[https://openframeworks.cc/ofBook/chapters/image_processing_computer_vision.html](https://openframeworks.cc/ofBook/chapters/image_processing_computer_vision.html)
